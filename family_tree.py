import re


class Family():
    """
    class Family that has people & relationships.
    """

    people = {}
    relation = []
    relationships = {}


class Person(Family):
    """
    class People to perform queries,
    like adding a person, finding a person.
    Inherits Family class.
    """

    def __str__(self):
        """
        dunder string method to represent object.
        """
        return self.name

    def add_person(self, string):
        """
        Adds a person to self.people.
        """
        p = Person()
        p.name = string.lower()
        p.id = re.sub('[^0-9A-Za-z]', '', p.name)
        key = p.id

        if key not in self.people:
            self.people[key] = p
            return True
        else:
            return False, "already exists"

    def find_person(self, name):
        """
        Tries to find a person matching the 'name' argument.
        """
        # First, search in ids
        if name in self.people:
            return self.people[name]
        # Ancestor not found in 'id', maybe it's in the 'name' field?
        for p in self.people.values():
            if p.name == name:
                return p
        return None


class Relationship(Person):
    """
    class Relationship to create & update a relationship.
    Inherits Person class which inherits Family class.
    """

    def add_relation(self, relation):
        if relation not in self.relation:
            self.relation.append(relation)
            return True
        else:
            return False, "already exists"

    def connect(self, person_1, person_2, relationship):
        """
        connect method establish the relationship between 2 people
        """
        p1 = self.find_person(person_1.lower())
        p2 = self.find_person(person_2.lower())
        relationship = relationship.lower()
        if relationship in self.relation:
            if p1 and p2:
                if p1.name in self.relationships.keys():
                    if relationship in self.relationships[p1.name]:
                        if p2.name in self.relationships[p1.name][relationship]:
                            return False, "already exists"
                        else:
                            self.relationships[p1.name][relationship].append(
                                p2.name)
                            return True
                    else:
                        self.relationships[p1.name][relationship] = [p2.name]
                        return True
                elif p2.name in self.relationships.keys():
                    if relationship in self.relationships[p2.name]:
                        if p1.name in self.relationships[p2.name][relationship]:
                            return False, "already exists"
                else:
                    self.relationships[p1.name] = {relationship: [p2.name]}
                    return True
            else:
                return False, "first add a person to make relationship"
        else:
            return False, "first add a relation to make a relationship"


class FamilyTree(Relationship):
    """
    class FamilyTree to create a family tree.
    Inherits Relationship class, and has all properties
    of above classes (multi-level inheritance).
    """

    def count_kids(self, name, string):
        """
        counts the kids of a person,
        only 1 generation.
        """
        if string.lower() in self.relation:
            person = self.find_person(name)
            if person:
                try:
                    kids = self.relationships[name][string.lower()]
                    return True, len(kids), kids
                except KeyError:
                    return False, "first add a relation to make a relationship"
            else:
                return False, "person not found"
        return False, "relation not found"

    def count_all_kids(self, name, string):
        """
        counts all kids of a person,
        all the successing generation.
        """
        status = self.count_kids(name, string)[0]
        if status:
            counter, kids = self.count_kids(name, string)[1:]
            for kid in kids:
                flag = self.count_kids(kid, string)[0]
                if flag:
                    counter += self.count_kids(kid, string)[1]
            return True, counter
        else:
            return False, "person/relation not found"


def execute_query():
    """
    method to execute the the queries.
    eg: family-tree add person <name>
    (do not use <> brackets).
    """
    query = ''
    FT = FamilyTree()
    print("Type 'q' or 'quit' to exit! \n To get queries type 'get queries' \n")

    while query not in ('q', 'quit'):
        query = input('Write Query:').lower()

        if query.startswith("family-tree"):
            query = query.replace('family-tree', '').strip()

            if query.startswith("add person"):
                query = query.replace('add person', '').strip()
                print(FT.add_person(query))

            elif query.startswith("add relation"):
                query = query.replace('add relation', '').strip()
                print(FT.add_relation(query))

            elif query.startswith("connect"):
                query = query.replace('connect', '').strip().split()
                print(FT.connect(query[-1], query[0], query[-3]))

            elif query.startswith("count all"):
                query = query.replace('count all', '').strip()

                if query.startswith('sons'):
                    status = FT.count_kids(
                        query.split()[-1], query.split()[0][:-1])[0]
                    if status:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1])[1])
                    else:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1]))

                elif query.startswith('daughters'):
                    status = FT.count_kids(
                        query.split()[-1], query.split()[0][:-1])[0]
                    if status:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1])[1])
                    else:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1]))

            elif query.startswith("count"):
                query = query.replace('count', '').strip()

                if query.startswith('sons'):
                    status = FT.count_kids(
                        query.split()[-1], query.split()[0][:-1])[0]
                    if status:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1])[1])
                    else:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1]))

                elif query.startswith('daughters'):
                    status = FT.count_kids(
                        query.split()[-1], query.split()[0][:-1])[0]
                    if status:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1])[1])
                    else:
                        print(FT.count_kids(query.split()
                                            [-1], query.split()[0][:-1]))

            else:
                print("Wrong Query!")

        elif query.startswith("get queries"):
            print(
                '''
                1. Adding a person in family-tree

                    - family-tree add person <name>

                2. Adding a relation in family-tree

                    - family-tree add relaton <relation_name>

                3. Adding relationship between 2 people

                    - family-tree connect <name_1> as <relationship> of <name_2>

                4. Getting the count kids (1 generation)

                    - family-tree count sons of <name>

                5. Getting the count of all kids (all successing generations)

                    - family-tree count all sons of <name>
                '''
            )

        elif query.startswith(("q", "quit")):
            print("Exiting!")

        else:
            print("Wrong Query!")


# initializing the main function
if __name__ == '__main__':
    execute_query()
