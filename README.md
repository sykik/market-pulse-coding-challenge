# Instruction to run code:
1. Install all dependancies by `pip -r install requirements.txt`
2. Write the queries in space seperated values just like mentioned in example.
3. Person or Relation name can't be space seperated, like 'satyam trivedi' isn't allowed,
instead use like this 'satyam-trivedi' or 'satyam_trivedi'.
4. Run family_tree.py
5. To run test execute `pytest` in folder directory in terminal.

# Problem Statement:

## Backend Coding Challenge #2

## Instruction
1. This is a timed coding challenge. You have maximum 2 hours to finish this.
2. You can use any programming language / framework of your choice
3. We want to be impressed with your OOPs / Functional programming skills, design skills & coding style
4. Unit tests are a big plus one
5. Incomplete or non-working codes will be rejected


# Build a Family Tree

Design a command line system which can help a user define his family tree & be able to fire a few interesting queries.


# Define a family tree:

## Our command line tool (family-tree) can have the following options:

`family-tree add person <name>`
eg: family-tree add Amit Dhakad

`family-tree add relationship <name>`
eg: family-tree add relationship father
eg: family-tree add relationship son

`family-tree connect <name 1> as <relationship> of <name 2>`
eg: family-tree connect Amit Dhakad as son of KK Dhakad


# Interesting Queries:

We can limit the assignment to only the following queries:

`family-tree count sons of <name>`
This should return count of sons

`family-tree count all daughters of <name>`
This should return count of all daughters, including great grand daughters of name


# Example that we will try to run:

## Our Family Tree:
Please ignore the DoB
Pink = Females

![](https://paper-attachments.dropbox.com/s_EF29991D0AA6DE46D36E74E72967D515DCC046D50947C1F0138B33E1FC266DD5_1568978587644_Shakespeare_Family_Tree.png)



# Our Queries and Answers:
`family-tree count sons of William`

# Answer 1

`family-tree count sons of John`

# Answer 4

`family-tree count all daughters of Mary Arden`

# Answer 7

`family-tree count all daughters of Anne Hathaway`

# Answer 3


## What can impress us:
1. Scalable Architecture
2. Good Design
3. Clean code
4. Unit tests
5. Frequent commits to git repo


## What not to do:
1. Hardcoding
2. Over-engineer the solution
