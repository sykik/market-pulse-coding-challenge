from os import path
import sys
import unittest

#linking parent directory path to current directory
sys.path.append(path.join(path.dirname(__file__), '..'))
import family_tree


class TestFamilyTree(unittest.TestCase):
    ft = family_tree.FamilyTree()
    mock_person_1, mock_person_2 = "sam", "pikachu"
    mock_relation = "son"

    def test_family_tree_add_person(self):
        output = self.ft.add_person(self.mock_person_2)
        expected_output = True
        self.assertEqual(output, expected_output)

        output = self.ft.add_person(self.mock_person_1)
        expected_output = True
        self.assertEqual(output, expected_output)

        output = self.ft.add_person(self.mock_person_1)
        expected_output = (False, 'already exists')
        self.assertEqual(output, expected_output)


    def test_family_tree_add_relation(self):
        output = self.ft.add_relation(self.mock_relation)
        expected_output = True
        self.assertEqual(output, expected_output)

        output = self.ft.add_relation(self.mock_relation)
        expected_output = (False, 'already exists')
        self.assertEqual(output, expected_output)
        self.relation_list = self.ft.relation
        self.relationship_dict = self.ft.relationships

    def test_family_tree_add_relationconnect(self):
        output = self.ft.connect(
                        self.mock_person_1,
                        self.mock_person_2,
                        self.mock_relation)
        expected_output = True
        self.assertEqual(output, expected_output)

        output = self.ft.connect(
                        self.mock_person_1,
                        self.mock_person_2,
                        self.mock_relation)
        expected_output = (False, 'already exists')
        self.assertEqual(output, expected_output)

    def test_family_tree_count_kids(self):
        output = self.ft.count_kids(self.mock_person_1, self.mock_relation)
        expected_output = (True, 1, [f'{self.mock_person_2}'])
        self.assertEqual(output, expected_output)

        output = self.ft.count_kids(self.mock_person_2, self.mock_relation)
        expected_output = (False, 'first add a relation to make a relationship')
        self.assertEqual(output, expected_output)

    def test_family_tree_count_all_kids(self):
        output = self.ft.count_all_kids(self.mock_person_1, self.mock_relation)
        expected_output = (True, 1)
        self.assertEqual(output, expected_output)

        output = self.ft.count_all_kids(self.mock_person_2, self.mock_relation)
        expected_output = (False, 'person/relation not found')
        self.assertEqual(output, expected_output)
